build:
	docker build --build-arg COLOR=yellow -t flower:yellow .

start-docreg:
	docker run -d -p 5000:5000 --name registry-srv -e REGISTRY_STORAGE_DELETE_ENABLED=true registry:2
	@echo -e "\nTo push to the local docker registry:"
	@echo "  docker tag myapp:v1 localhost:5000/myapp:v1"
	@echo "  docker push localhost:5000/myapp:v1"

stop-docreg:
	docker stop registry-srv
	docker rm registry-srv
