#
# build
#
FROM golang:1.13.3 as builder
COPY src .
RUN go test
RUN go build -o /app main.go

#
# package up only the app
#
FROM alpine:3.10
# set color from build-ars on docker build command

ARG COLOR
ENV COLOR ${COLOR:-yellow}

EXPOSE 80
COPY --from=builder /app .
CMD ["./app"]