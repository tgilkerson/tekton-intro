# Tekton Introduction

## History

Google [announced Knative](https://cloudplatform.googleblog.com/2018/07/bringing-the-best-of-serverless-to-you.html) in July 2018 which included **Knative Build** - a rudimentary framework for building the  Knative serverless container images. However, a number of different Kubernetes tools quickly latched onto Knative build as a more general framework for native Kubernetes CD/CD - even though this wasn’t the original intent. So Knative build-pipeline (now Tekton) was born to provide an open, full-featured and standardized native Kubernetes CD solution - where Knative Build was not. 

## What is Tekton

Tekton is a cloud native project under the [CNCF](https://landscape.cncf.io/selected=tekton-pipelines). It provides Kubernetes resources for declaring CI/CD-style pipelines.

![](img/Tekton-Pipelines-CNCF-Landscape.png)
> Tekton CNCF Landscape Card

The [Tekton GitHub Organization](https://github.com/tektoncd) contains the several repos. In this introduction we will touch on the following:

* **Pipelines** -  A CI/CD Pipeline. Includes concept such as `Pipeline`, `PipelineRun`, `Task`, `TaskRun` and `PipelineResource`
  * Run on Kubernetes
  * Have Kubernetes clusters as a first class type
  * Use containers as their building blocks
* **Triggers** - Triggers is a Kubernetes Custom Resource Definition (CRD) controller that allows you to extract information from events payloads (a "trigger") to create Kubernetes resources, in our case we will trigger a pipeline.
* **CLI** - The `tkn` CLI for interacting with Tekton!
* **Dashboard** - Tekton Dashboard is a general purpose, web-based UI for Tekton Pipelines

## Tekton concepts

* **Pipeline** - a collection of *PipelineResources*, *Parameters* and one or more *Tasks*
* **Task** - a collection of *Steps* run sequentially 
* **Step** - a container with a common mounted `/workspace` folder
* **TaskRun** - used to run a *Task*. It is auto-created by a *PipelineRun* for each *Task* in a *Pipeline*, but a *TaskRun* can also be manually created and used if you want to run a *Task* outside of a *Pipeline*
* **PipelineRun** - used to run a *Pipeline*. A *PipelineRun* will trigger the creation of a *TaskRun* for each *Task* in the *Pipeline* being run.
* **PipelineResource** - provide input and output to a *Pipeline*. The most common are a Git repository for input and a container image for output
* **Parameters** - used to declare input parameters that must be supplied to the *Pipeline* during a *PipelineRun*. *Pipeline* parameters can be used to replace template values in *Pipeline*'s *Tasks*

As you can see Tekton has many interrelated concepts, which can be confusing so perhaps a few drawings will help.

![](img/interaction-simple.png)

> The drawing above is a simplified view of the relationship between the main Tekton entities.  The *Eventlistener* is an HTTP endpoint listening for a webhook event.  When the event is received a *PipelineRun* is created which in turn will run one or more *TaskRuns*. The order of the *TaskRuns* can be controlled in the *PipelineRun* by specifying the `runAfter` property.    

---

![](img/interaction-detailed.png)

> The drawing above expands on our first drawing in order to show more detail relationships. The *Pipeline* entity seen here is really the specification for a *Pipeline*. It contains definitions for resources, parms and makes reference to *Tasks*. The *Task* seen here is also a definition of a series of steps to be executed. The actual values for resources and the parms will be supplied at runtime by the *PipelineRun*. The *EventListner* and related concepts are described below.

* **TriggerTemplate** - Templates resources to be created (e.g. Create *PipelineResources* and *PipelineRun*)
* **TriggerBinding** - Validates events and extracts payload fields
* **EventListener** - Connects *TriggerBindings* and *TriggerTemplates* into an addressable endpoint (the event sink). It uses the extracted event parameters from each *TriggerBinding* (and any supplied static parameters) to create the resources specified in the corresponding *TriggerTemplate*

---

## Hands On

This article is written as a series of hands-on exercises. We will be running pipelines in our local Kubernetes cluster.  Before we get started we need to take care of some pre-requisites and install some tools.

### Pre-requisites

1. A Kubernetes cluster version 1.11 or later.  I suggest [Docker Desktop for Mac and Windows](https://www.docker.com/products/docker-desktop) or if you are running Linux [microk8s](https://microk8s.io/).
1. Grant cluster-admin permissions to the current user.

    In my case I looked in the `users` section of my `~/.kube/config` file and because I am using Docker desktop for the Mac I see my user name is `docker-desktop` and ran the following command:

    ```bash
    # docker desktop
    kubectl create clusterrolebinding cluster-admin-binding \
    --clusterrole=cluster-admin \
    --user=docker-desktop
    ```

    > if you are not using Docker Desktop replace `docker-desktop` with your current username.

---

### NGINX Ingress controller

The following **Mandatory Command** is required for all deployments.

```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/mandatory.yaml
```

#### Provider Specific Steps

In addition to the mandatory command above run the command below that applies to your workstation.

If using Docker for Mac:

```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/cloud-generic.yaml
```

If using minikube:

```bash
minikube addons enable ingress
```

Other:

```bash
kubectl apply -f \
    https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/provider/cloud-generic.yaml

```

For more detail see the [install guide](https://kubernetes.github.io/ingress-nginx/deploy/#provider-specific-steps)
 
Note, it is assumed your user has the `cluster-admin` role applied above. 

To verify run the following and look for the status of `Running`

```bash
kubectl get pods --all-namespaces -l app.kubernetes.io/name=ingress-nginx --watch
```

### `kubectl` CLI

Install the `kubectl` CLI and populate the `~/.kube/config` with the config from your Kubernetes dashboard.

```bash
# on Ubuntu
sudo apt-get update && sudo apt-get install -y apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubectl

# or using snap
sudo snap install kubectl --classic

# on Mac
brew install kubectl

# or Other
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
```

---

### Kubernetes Dashboard

Install the Kubernetes dashboard UI

```bash
# Deploy the UI
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-beta4/aio/deploy/recommended.yaml

# needed for login
DASH_SECRET=$(kubectl get serviceAccounts -o jsonpath="{.items[0].secrets[0].name}")

# cut/past the token shown form this command
kubectl describe secrets/$DASH_SECRET

```

> Run `kubectl proxy` in a terminal window to make the dashboard available at http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/

---

### `tkn` CLI

Install the `tkn` CLI tools.  For other platforms see the [Tekton cli page](https://github.com/tektoncd/cli) for detail instructions.

```bash
# Mac
brew tap tektoncd/tools
brew install tektoncd-cli

# Linux
# Get the tar.xz
curl -LO https://github.com/tektoncd/cli/releases/download/v0.4.0/tkn_0.4.0_Linux_x86_64.tar.gz
# Extract tkn to your PATH (e.g. /usr/local/bin)
sudo tar xvzf tkn_0.4.0_Linuxg_x86_64.tar.gz -C /usr/local/bin tkn
```

---

### `ngrok` CLI (optional/recommended)

[ngrok](https://ngrok.com/download) will provide a public URL for sending webhooks to your Tekton event listeners. This is optional, we can use `curl` to mimic a webhook request but `ngrok` has a nice UI that allows you to view the traffic and is good for debugging.   

```bash
# Mac 
brew cask install ngrok

# or linux
# download form https://ngrok.com/download
unzip /path/to/ngrok.zip

# or with snap
sudo snamp install ngrok
```

In a separate terminal run `./ngrok http 80` 

---

### Setup Local Docker Registry

For our purposes we will create a local docker registry for pushing container images to. And to make things simple we will not secure the registry. As a result we will not need to setup docker credentials in our Tekton pipelines. We will leave this for a future exercise.

To make things easy I have provide some helper scripts that can be used to start and stop your local Docker registry.  To get started clone this repo and navigate to the project root. All command in this article assume you are in the project root folder.

* Start registry run `make start-docreg`
* Stop registry run `make stop-docreg`

The following shows what it looks like to start the registry:

```bash
# Start
$ make start-docreg
docker run -d -p 5000:5000 --name registry-srv -e REGISTRY_STORAGE_DELETE_ENABLED=true registry:2
417b6528d56b509508b5540e28c903ea11c6fe20996bcacbdd130885f91290d0
```

To make this accessible from within your Kubernetes cluster you need to modify your `/etc/hosts` file.  First we need to find the IP address of our workstation.  On a Mac you can do the following:

```bash
$ ifconfig | grep "inet"
inet address  192.168.1.149
```

So in my case we can select `192.168.1.149`, which corresponds to my laptop network card. The main thing to avoid is using `127.0.0.1 (localhost)` which will not be available from with our Kubernetes environment. 

Now in order to access this IP, we should use a locally resolvable hostname. This can be done by setting our `/etc/hosts` file to enable local DNS resolution. Let’s add an entry which corresponds to our local interface as follows:

```bash
##
# Host Database
#
# localhost is used to configure the loopback interface
# when the system is booting.  Do not change this entry.
##
127.0.0.1       localhost
255.255.255.255 broadcasthost
::1             localhost

# Added by Docker Desktop
# To allow the same kube context to work on the host and the container:
127.0.0.1 kubernetes.docker.internal
# End of section

192.168.1.149  host.docker.local
```

Next we need to allow insecure-registries within our docker-for-desktop daemon by editing the `~/.docker/daemon.json` file and add `host.docker.local:5000` to the `insecure-registries` collection as shown below:

```bash
{
  "experimental" : false,
  "insecure-registries" : [
    "host.docker.local:5000"
  ],
  "debug" : true
}
```

To verify let's push an image to our local Docker registry:

```bash
# Verify by pushing to the local docker registry:
docker pull busybox:latest
docker tag busybox:latest localhost:5000/busybox:latest
docker push localhost:5000/busybox:latest
curl http://host.docker.local:5000/v2/_catalog
```

---

### Tekton Pipelines

To install the Tekton pipeline engine apply the `release.yaml` as shown below.  For more information on how to install Tekton see the [Tekton install doc](https://github.com/tektoncd/pipeline/blob/master/docs/install.md)

```bash
kubectl apply --filename https://storage.googleapis.com/tekton-releases/pipeline/latest/release.yaml

# Watch the components until all is ready showing a running status
watch kubectl get pods --namespace tekton-pipelines
```

---

### Tekton Triggers

To install the Tekton Triggers apply the `release.yaml` as shown below. 

```bash
kubectl apply --filename https://storage.googleapis.com/tekton-releases/triggers/latest/release.yaml

# then watch the pods untll all are running
$ watch kubectl get pods --namespace tekton-pipelines
NAME                                           READY   STATUS    RESTARTS   AGE
tekton-pipelines-controller-756f4f448f-jwcpw   1/1     Running   2          3d21h
tekton-pipelines-webhook-79fcc6c768-vvl6w      1/1     Running   2          3d21h
tekton-triggers-controller-6d8fd9596b-f4ckd    1/1     Running   0          52s
tekton-triggers-webhook-5f8d569d5f-x9ks6       1/1     Running   0          52s
```

---

## Exercise 0 - Clone Project

All commands assume you have cloned this project and navigated to the project root as shown:

```bash
git clone https://gitlab.com/tgilkerson/tekton-intro.git
cd tekton-intro
```

This section is based on the [this tutorial](https://github.com/tektoncd/pipeline/blob/master/docs/tutorial.md)found on the Tekton project page. 

---

## Exercise 1 - Task and TaskRun hello world

This is a "hello world" exercise. We will create a simple `Task` that has one `step` that will print "hello world".  The `Task` defines the work that needs to be executed i.e. print "hello world" and the `TaskRun` runs that task.  

Review the [exercises/ex1-tasks.yaml](exercises/ex1-tasks.yaml) source file to see the `Task` and `TaskRun` that we will use for our first exercise.

To run the exercise execute the following commands:

* `kubectl create namespace ex1`
* `kubectl -n ex1 apply -f exercises/ex1-tasks.yaml`

```bash
$ kubectl create namespace ex1
namespace/ex1 created

$ kubectl -n ex1 apply -f exercises/ex1-tasks.yaml
task.tekton.dev/hello-world-task created
taskrun.tekton.dev/hello-world-taskrun created
```

After we allow some time for the image specified in the task to download and run we can review the status.  There are several many ways to review the status. The most basic way is to use `kubectl`.  Here are some examples of how to check the status:

* `kubectl -n ex1 get tasks`
* `kubectl -n ex1 get taskruns`
* `kubectl -n ex1 get taskrun hello-world-taskrun -o yaml`

```text
$ kubectl -n ex1 get tasks
NAME               AGE
hello-world-task   2m2s

$ kubectl -n ex1 get taskruns
NAME                  SUCCEEDED   REASON      STARTTIME   COMPLETIONTIME
hello-world-taskrun   True        Succeeded   29s         23s

$ kubectl -n ex1 get taskrun hello-world-taskrun -o yaml
apiVersion: tekton.dev/v1alpha1
kind: TaskRun
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"tekton.dev/v1alpha1","kind":"Task","metadata":{"annotations":{},"name":"hello-world-task","namespace":"ex1"},"spec":{"steps":[{"args":["hello world"],"command":["echo"],"image":"ubuntu","name":"echo"}]}}
  creationTimestamp: "2019-10-25T18:36:43Z"
  generation: 1
  labels:
    tekton.dev/task: hello-world-task
  name: hello-world-taskrun
  namespace: ex1
  resourceVersion: "53235"
  selfLink: /apis/tekton.dev/v1alpha1/namespaces/ex1/taskruns/hello-world-taskrun
  uid: 643a44ef-f756-11e9-a924-025000000001
spec:
  inputs: {}
  outputs: {}
  podTemplate: {}
  serviceAccountName: ""
  taskRef:
    kind: Task
    name: hello-world-task
  timeout: 1h0m0s
status:
  completionTime: "2019-10-25T18:36:49Z"
  conditions:
  - lastTransitionTime: "2019-10-25T18:36:49Z"
    message: All Steps have completed executing
    reason: Succeeded
    status: "True"
    type: Succeeded
  podName: hello-world-taskrun-pod-eb6133
  startTime: "2019-10-25T18:36:43Z"
  steps:
  - container: step-echo
    imageID: docker-pullable://ubuntu@sha256:a7b8b7b33e44b123d7f997bd4d3d0a59fafc63e203d17efedf09ff3f6f516152
    name: echo
    terminated:
      containerID: docker://3e5767c11362771a8f0573b398a00fe012bbf8167b721cf9280cbf420257ad0d
      exitCode: 0
      finishedAt: "2019-10-25T18:36:49Z"
      reason: Completed
      startedAt: "2019-10-25T18:36:48Z"
```

Or we can use the Tekton CLI

* `tkn -n ex1 task list`
* `tkn -n ex1 taskrun list`
* `tkn -n ex1 taskrun describe hello-world-taskrun`

```bash
$ tkn -n ex1 task list
NAME               AGE
hello-world-task   7 minutes ago

$ tkn -n ex1 taskrun list
NAME                  STARTED         DURATION    STATUS
hello-world-taskrun   7 minutes ago   6 seconds   Succeeded

$ tkn -n ex1 taskrun describe hello-world-taskrun
Name:        hello-world-taskrun
Namespace:   ex1
Task Ref:    hello-world-task

Status
STARTED         DURATION    STATUS
8 minutes ago   6 seconds   Succeeded

Input Resources
No resources

Output Resources
No resources

Params
No params

Steps
NAME
echo
```

To view the logs:

* `POD_NAME=$(kubectl -n ex1 get pods -o=jsonpath='{.items[0].metadata.name}')`
* `kubectl -n ex1 logs $POD_NAME`
* `tkn -n ex1 taskrun logs hello-world-taskrun`

```bash
# use kubectl to display pod logs
POD_NAME=$(kubectl -n ex1 get pods -o=jsonpath='{.items[0].metadata.name}') 
$ kubectl -n ex1 logs $POD_NAME
hello world

# display logs using Tekton CLI
tkn -n ex1 taskrun logs hello-world-taskrun
[echo] hello world
```

As you can see in the logs of the `TaskRun` it did indeed print "hello world"!

Cleanup Exercise:

```bash
# delete the namespace
kubectl delete ns ex1
```

---

## Exercise 2 - Build image from a git repo

In the exercise we have a single `Task` with one `step` but this time we are going to do some work.  

* Pull source from a git repo using an input `PipelineResource` of `type: git`
* Build a container image
* Push the image to our registry using an output `PipelineResource` of `type: image`  

If you recall from above a `step` is an container that runs a command. In this case we want to build an image from within a container. To accomplish this we will use [Kaniko](https://github.com/GoogleContainerTools/kaniko).  Specifically we will use the Kaniko `gcr.io/kaniko-project/executor:v0.13.0` image for the `step`.

Review the [ex2-taskrun-build.yaml](exercises/ex2-taskrun-build.yaml) source file to see the resources use in this exercise.

Create a new name space for exercise 2 and then apply the yaml source.

* `kubectl create namespace ex2`
* `kubectl -n ex2 apply -f exercises/ex2-taskrun-build.yaml`

```bash
$ kubectl create namespace ex2
namespace/ex2 created

$ kubectl -n ex2 apply -f exercises/ex2-taskrun-build.yaml
pipelineresource.tekton.dev/app-git created
pipelineresource.tekton.dev/app-image created
task.tekton.dev/build-image-from-git-task created
taskrun.tekton.dev/build-image-from-git-taskrun created

```

Watch the TaskRun until the stats is successful or watch the logs:

* `watch tkn -n ex2 taskrun list`
* `tkn -n ex2 taskrun logs build-image-from-git-taskrun --follow`

**Note:** The fist time may take several minutes because several images need to download. After that it faster, but the build still takes longer than you might expect. Maybe this is the result of docker-in-docker.


```bash
# Interactive watch
$ watch tkn -n ex2 taskrun list

Every 2.0s: tkn -n ex2 taskrun list

NAME                           STARTED        DURATION   STATUS
build-image-from-git-taskrun   1 minute ago   ---        Running

# Follow the logs
$ tkn -n ex2 taskrun logs build-image-from-git-taskrun --follow
...
[build-and-push] INFO[0125] cmd: /bin/sh                                 
[build-and-push] INFO[0125] args: [-c go build -o /app main.go]          
[build-and-push] INFO[0126] Taking snapshot of full filesystem...        
[build-and-push] INFO[0127] Saving file /app for later use.              
[build-and-push] INFO[0127] Deleting filesystem...                       
[build-and-push] INFO[0128] Downloading base image alpine:3.9            
[build-and-push] INFO[0128] Error while retrieving image from cache: getting file info: stat /cache/sha256:bf1684a6e3676389ec861c602e97f27b03f14178e5bc3f70dce198f9f160cce9: no such file or directory 
[build-and-push] INFO[0128] Downloading base image alpine:3.9            
[build-and-push] INFO[0129] Skipping unpacking as no commands require it. 
[build-and-push] INFO[0129] Taking snapshot of full filesystem...        
[build-and-push] INFO[0129] ARG COLOR                                    
[build-and-push] INFO[0129] ENV COLOR ${COLOR:-yellow}                   
[build-and-push] INFO[0129] EXPOSE 80                                    
[build-and-push] INFO[0129] cmd: EXPOSE                                  
[build-and-push] INFO[0129] Adding exposed port: 80/tcp                  
[build-and-push] INFO[0129] COPY --from=builder /app .                   
[build-and-push] INFO[0129] Taking snapshot of files...                  
[build-and-push] INFO[0129] CMD ["./app"] 
...
```

Confirm that the image was pushed to our local registry:

```bash
$ curl http://host.docker.local:5000/v2/_catalog
{"repositories":["flower"]}

$ curl http://host.docker.local:5000/v2/flower/tags/list
{"name":"flower","tags":["latest"]}

```

Cleanup this exercise:

```bash
# remove stuff so you can rerun this exercise
tkn -n ex2 taskrun delete build-image-from-git-taskrun

# or delete the namespace if you are all done with this exercise
kubectl delete ns ex2
```

---

## Exercise 3 - Task ordering and deployment

This exercise builds on the previous.  Just as before we will pull our source code and build an image but this time we will deploy the image into the cluster.

* Task 1
  * Build a container image
  * Push the image to our registry using an output `PipelineResource` of  of `type: image`
* Task 2 (`runAfter` Task 1)
  * Deploy image into our cluster

Now that we have more than one task we need a way to specify the order or execution.  As you recall a `Pipeline` defines a list of tasks to execute, the order of execution and if any outputs should be used as inputs of a following task.  The Tasks in a Pipeline can be connected and run in a graph, specifically a *Directed Acyclic Graph or DAG*. Each of the Pipeline Tasks is a node, which can be connected with an edge (i.e. a Graph) such that one will run before another (i.e. Directed), and the execution will eventually complete (i.e. **Acyclic**, it will not get caught in infinite loops).  This is accomplished primarily with the `runAfter` and `from` clauses. See the [Tekton documentation](https://github.com/tektoncd/pipeline/blob/master/docs/pipelines.md#ordering) for more detail about ordering.

Review the [ex3-pipelinerun.yaml](exercises/ex3-pipelinerun.yaml) source file to see the resources use in this exercise. For deployment this [ex3-deployment.yaml](exercises/ex3-deployment.yaml) source file is applied. Notice that the `replaceme` image tag specified in the deployment manifest is replaced in the pipeline before it is applied.

Create a new name space for exercise 3 and then apply the yaml source.

* `kubectl create namespace ex3`
* `kubectl -n ex3 apply -f exercises/ex3-rbac.yaml`
* `kubectl -n ex3 apply -f exercises/ex3-pipelinerun.yaml`

```bash
$ kubectl create namespace ex3
namespace/ex3 created

$ kubectl -n ex3 apply -f exercises/ex3-rbac.yaml
serviceaccount/ex3-deployment-manager created
role.rbac.authorization.k8s.io/ex3-deployment-manager created
rolebinding.rbac.authorization.k8s.io/ex3-deployment-manager created

$ kubectl -n ex3 apply -f exercises/ex3-pipelinerun.yaml
pipelineresource.tekton.dev/tekton-into-git-resource created
pipelineresource.tekton.dev/tekton-into-image-resource created
task.tekton.dev/build-image-from-git-task created
task.tekton.dev/deploy-using-kubectl-task created
pipeline.tekton.dev/build-deploy-pipeline created
pipelinerun.tekton.dev/build-deploy-pipeline-run-1 created
```

Watch the TaskRun until the stats is successful or watch the logs:

* `watch tkn -n ex3 pipelinerun list`
* `tkn -n ex3 pipelinerun logs build-deploy-pipeline-run-1  --follow`

Verifiy deployment:

After the pipeline run is successful verify by listing the pods.  Notice we have a completed pod for each pipeline task and our app is deployed!

```bash
$ kubectl -n ex3 get deployments
NAME             READY   UP-TO-DATE   AVAILABLE   AGE
ex3-deployment   1/1     1            1           7m48s
```

Cleanup:

```bash
# remove stuff so you can rerun this exercise
kubectl -n ex3 delete pipelinerun --all
# or delete the namespace if you are all done with this exercise
kubectl delete ns ex3
```

---

## Exercise 4 - Simple Triggers

In this example we will consume an event and use it to trigger a simple pipeline.

Review the [ex4-triggers.yaml](exercises/ex4-triggers.yaml) source file to see the resources use in this exercise.

### Apply Example 4

```bash
kubectl create ns ex4
kubectl -n ex4 apply -f exercises/ex4-triggers.yaml
```

Check required pods and services are available and healthy

```bash
$ kubectl -n ex4 get pods
NAME                           READY   STATUS    RESTARTS   AGE
el-listener-86c49956bd-9qkrx   1/1     Running   0          2m43s

$ kubectl -n ex4 get svc
NAME          TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE
el-listener   ClusterIP   10.103.164.244   <none>        8080/TCP   104s
```

We don't have an ingress setup yet so let's expose the listener:

```bash
EL_POD=$(kubectl -n ex4 get pod -o=name -l eventlistener=listener)
kubectl -n ex4 port-forward $EL_POD 8080

# You will see
Forwarding from 127.0.0.1:8080 -> 8080
Forwarding from [::1]:8080 -> 8080
```

Send message to event listener:

```bash
# MR
curl -X POST \
  http://localhost:8080 \
  -H 'Content-Type: application/json' \
  -d '{
  "object_kind": "merge_request",
	"object_attributes": {
    "source_branch": "mrtest",
    "target_branch": "master"
  },
	"project": { "git_http_url": "https://gitlab.com/tgilkerson/tekton-intro.git" }
}'
```

Watch the TaskRun until the stats is successful or watch the logs:

* `watch tkn -n ex4 pipelinerun list`
* `tkn -n ex4 pipelinerun logs USE_NAME_FROM_LIST`

```bash
PIPELINE_RUN=$(kubectl -n ex4 get pipelinerun -o=name | cut -d"/" -f 2)
tkn -n ex4 pipelinerun logs $PIPELINE_RUN
```

Cleanup 

```bash
# remove stuff so you can rerun this exercise
kubectl -n ex4 delete pods -l tekton.dev/pipeline=simple-pipeline
kubectl -n ex4 delete pipelinerun --all
# or delete the namespace if you are all done with this exercise
kubectl delete ns ex4
```

---

## Exercise 5 - Trigger a Pipeline

In this exercise we will trigger a pipeline with a GitLab merge request.  The pipeline will simply add a comment on the merge request.  

Review the [ex5-mr-trigger.yaml](exercises/ex5-mr-trigger.yaml) source file to see the resources use in this exercise.

### Update Secret

In order to call the GitLab API used to add the comment you will need a personal access token to the repo. This exercises assumes you have forked this repo and have an access token setup on your fork.  

Edit the [ex5-mr-trigger](exercises/ex5-mr-trigger) soruce file and replace the text `REPLACE-ME-WITH-YOUR-PERSONAL-ACCESS-TOKEN` with your actual access token. I suggest you make a token just for this exercise becase we will be displaying the token in a debug message to see the pipeline run.

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: gitlab-secret
type: Opaque
stringData:
  personalAccessToken: "REPLACE-ME-WITH-YOUR-PERSONAL-ACCESS-TOKEN" 
```

### Apply Example 5

```bash
kubectl create ns ex5
kubectl -n ex5 apply -f exercises/ex5-mr-trigger.yaml

# Verify
$ kubectl -n ex5 get pod
NAME                           READY   STATUS    RESTARTS   AGE
el-listener-58c67797d7-wjdlb   1/1     Running   0          3m34s
```

We don't have an ingress setup yet so let's expose the listener:

```bash
EL_POD=$(kubectl -n ex5 get pod -o=name -l eventlistener=listener)
$ kubectl -n ex5 port-forward $EL_POD 8080
Forwarding from 127.0.0.1:8080 -> 8080
Forwarding from [::1]:8080 -> 8080
```

We need a merge request for this exercise so go a head and create one that is accessable using your personal access token. Assuming GitLab is in the cloud and your cluster in running on a private (or on your desktop) we need a way to get the webhook event to your event listener. To do this we have two options:


### Option 1 - Use curl to mimic a web hook

The following curl command will nimic a web hook and can be used to trigger your pipeline, however the information in the json body need to correspond to the merge request you jsut created.  You should be able to get all the infomation below for `iid`, `source_branch`, `target_branch`, `projectd` and project `id` from the GitLab UI.  The following curl command has everything we need to initiate our pipeline. 


```bash
# sample merge request webhook event
curl -X POST \
  http://localhost:8080 \
  -H 'Content-Type: application/json' \
  -d '{
  "object_kind": "merge_request",
	"object_attributes": {
    "iid": 3
    "source_branch": "mrtest",
    "target_branch": "master"
  },
	"project": {
    "id": 15002192,
    "git_http_url": "https://gitlab.com/tgilkerson/tekton-intro.git" 
  }
}'
```

After you edited the curl command for your repo, run it and then watch the TaskRun until the stats is successful or watch the logs:

* `watch tkn -n ex5 pipelinerun list`
* `tkn -n ex5 pipelinerun logs USE_NAME_FROM_LIST`

```bash
PIPELINE_RUN=$(kubectl -n ex4 get pipelinerun -o=name | cut -d"/" -f 2)
tkn -n ex5 pipelinerun logs $PIPELINE_RUN
```

### Option 2 - use ngrok to proxy webhook event

To test it out with a git provider running on the cloud you can use `ngrok` to setup a tunnel to your laptop. e.g.:
* `ngrok http 8080`

```bash
$ ngrok http 8080
Session Status                online
Session Expires               7 hours, 59 minutes
Version                       2.3.35
Region                        United States (us)
Web Interface                 http://127.0.0.1:4040
Forwarding                    http://f9557082.ngrok.io -> http://localhost:8080
Forwarding                    https://f9557082.ngrok.io -> http://localhost:8080
Connections                   ttl     opn     rt1     rt5     p50     p90
                              0       0       0.00    0.00    0.00    0.00
```

`ngrok` provides a real-time web UI where you can introspect all of the HTTP traffic running over your tunnels. After you've started `ngrok`, just open `http://localhost:4040` in a web browser to inspect request details. See the [ngrok docs](https://ngrok.com/docs) for more detail.


Once you have the proxy up and running, create a new merge request in GitLab and then watch the TaskRun until the stats is successful or watch the logs:

* `watch tkn -n ex5 pipelinerun list`
* `tkn -n ex5 pipelinerun logs USE_NAME_FROM_LIST`

```bash
PIPELINE_RUN=$(kubectl -n ex5 get pipelinerun -o=name | cut -d"/" -f 2)
tkn -n ex4 pipelinerun logs $PIPELINE_RUN
```

### cleanup

```bash
# remove old pipelines but keep listner running
kubectl -n ex5 delete pods -l tekton.dev/pipeline=simple-pipeline
kubectl -n ex5 delete pipelinerun --all
# or delete the namespace if you are all done with this exercise
kubectl delete ns ex5
```


---

## Exercise 6 - Build MR

In this exercise we will:

* trigger a pipeline with a GitLab merge request
* do a docker build
* run unit tests and save report on volume
* add a comment on the merge request that includes a formated unit test report

Review the [ex6-mr-build.yaml](exercises/ex6-mr-build.yaml) source file to see the resources use in this exercise.

### Apply Example 6

* `kubectl create ns ex6`
* `kubectl -n ex6 apply -f exercises/ex6-mr-build.yaml`

```bash
kubectl create ns ex6
kubectl -n ex6 apply -f exercises/ex6-mr-build.yaml

# Verify
$ kubectl -n ex6 get pod
NAME                           READY   STATUS    RESTARTS   AGE
el-listener-58c67797d7-wjdlb   1/1     Running   0          3m34s
```

We don't have an ingress setup yet so let's expose the listener:

```bash
EL_POD=$(kubectl -n ex6 get pod -o=name -l eventlistener=listener)
kubectl -n ex6 port-forward $EL_POD 8080
```

Let's reuse the merge request created in the previous exercise.  Assuming the merge request is till open, we can trigger our pipeline with a curl command.

```bash

```bash
# sample merge request webhook event
curl -X POST \
  http://localhost:8080 \
  -H 'Content-Type: application/json' \
  -d '{
  "object_kind": "merge_request",
	"object_attributes": {
    "iid": 3
    "source_branch": "mrtest",
    "target_branch": "master"
  },
	"project": {
    "id": 15002192,
    "git_http_url": "https://gitlab.com/tgilkerson/tekton-intro.git" 
  }
}'
```

After you edited the curl command for your repo, run it and then watch the TaskRun until the stats is successful or watch the logs:

* `watch tkn -n ex6 pipelinerun list`
* `tkn -n ex6 pipelinerun logs USE_NAME_FROM_LIST`

```bash
PIPELINE_RUN=$(kubectl -n ex6 get pipelinerun -o=name | cut -d"/" -f 2)
tkn -n ex6 pipelinerun logs $PIPELINE_RUN
```

Confirm that the image was pushed to our local registry:

```bash
$ curl http://host.docker.local:5000/v2/_catalog
{"repositories":["flower"]}

$ curl http://host.docker.local:5000/v2/flower/tags/list
{"name":"flower","tags":["latest"]}

```

Cleanup: 

```bash
kubectl -n ex6 delete pods -l tekton.dev/pipeline=simple-pipeline
kubectl -n ex6 delete pipelinerun --all
# or delete the namespace if you are all done with this exercise
kubectl delete ns ex6
```

---

## Exercise 7 - Tekton Dashboard

In this exercise we will:

* install the Tekton Dashboard
* create a task run from the UI
* create a pipeline run from the UI

Tekton Dashboard is a general purpose, web-based UI for Tekton Pipelines. It allows users to manage and view Tekton PipelineRuns and TaskRuns and the resources involved in their creation, execution, and completion. It also allows filtering of PipelineRuns and TaskRuns by label. For more detail see See the [Tekton Dashboard Github page](https://github.com/tektoncd/dashboard)

### Install Dashboard

```bash
kubectl apply --filename https://github.com/tektoncd/dashboard/releases/download/v0.2.1/dashboard-latest-release.yaml
```

Run the following and and wait for all the components to so a status of `Running`

```bash
watch kubectl get pods --namespace tekton-pipelines
```

### Setup Ingress for Dashboard

Make sure you have the ingress controller installed.  See `Pre-requisites` above for more detail on how to install the ingress controller.

Update the `tekton-dashboard-ingress.yaml` file with the IP address of your K8S host. In my case I am running on my workstation so I run the `ifconfig | grep inet` to find my IP Address.

```bash
# set IP with my-IP (ie run "ifconfig | grep inet" to find your ipaddress)
IP=replace-with-k8s-ip

# replace value in supplied yaml file with your ip  
# and save to a temp file
cat exercises/tekton-dashboard-ingress.yaml | sed 's/replaceme/'"$IP"'/g' > my-ingress.yaml

# apply the temp file
kubectl apply -f my-ingress.yaml
```

For example, if given `IP=10.0.0.54` then your `my-ingress.yaml` should look like this:

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: tekton-dashboard
  namespace: tekton-pipelines
spec:
  rules:
  - host: tekton-dashboard.10.0.0.54.nip.io
    http:
      paths:
      - backend:
          serviceName: tekton-dashboard
          servicePort: 9097
```

Now point your browser at the host specified in your `my-ingress.yaml`. It will look something like this:

```bash
open http://tekton-dashboard.10.0.0.54.nip.io
```

![](img/tekton_dashboard.png)

### Create a TaskRun and PipelineRun via UI

To create a TaskRun and a PipelineRun we first need to crate a task and a pipeline by executing the following commands:

```bash
kubectl create namespace ex7
kubectl -n ex7 apply -f exercises/ex7-ui.yaml
```

In the Tekton UI do the following:

* `Namespaces -> ex7`
* `Tekton -> Tasks` -  You should see `my-task` in the Task list
* `Tekton -> Pipelines` -  You should see
  * `simple-pipeline` - a simple pipeline
  * `parm-pipeline` - will prompt you for a parm value
  * `dag-pipeline` - will run a DAG example
* `Tekton -> Pipelines` - then click `Create PipelineRun` button
  * do this several time each time select a different pipeline
  * click the pipelinerun to see the steps

![](img/tekton_dashboard_steps.png)


---

## GitLab WebHook Samples

### Merge Request Event

```json

POST / HTTP/1.1
Content-Type: application/json
X-Gitlab-Event: Merge Request Hook
X-Gitlab-Token: 1234567
Connection: close
Host: 74cc816a.ngrok.io
Content-Length: 3620
X-Forwarded-For: 34.73.48.20

{
   "object_kind":"merge_request",
   "event_type":"merge_request",
   "user":{
      "name":"Tony Gilkerson",
      "username":"tgilkerson",
      "avatar_url":"https://secure.gravatar.com/avatar/f4889b181d1324246d7edae67ca17e99?s=80\u0026d=identicon"
   },
   "project":{
      "id":15002192,
      "name":"tekton-intro",
      "description":"An intro to tekton ci/cd",
      "web_url":"https://gitlab.com/tgilkerson/tekton-intro",
      "avatar_url":null,
      "git_ssh_url":"git@gitlab.com:tgilkerson/tekton-intro.git",
      "git_http_url":"https://gitlab.com/tgilkerson/tekton-intro.git",
      "namespace":"Tony Gilkerson",
      "visibility_level":20,
      "path_with_namespace":"tgilkerson/tekton-intro",
      "default_branch":"master",
      "ci_config_path":null,
      "homepage":"https://gitlab.com/tgilkerson/tekton-intro",
      "url":"git@gitlab.com:tgilkerson/tekton-intro.git",
      "ssh_url":"git@gitlab.com:tgilkerson/tekton-intro.git",
      "http_url":"https://gitlab.com/tgilkerson/tekton-intro.git"
   },
   "object_attributes":{
      "assignee_id":null,
      "author_id":3493733,
      "created_at":"2019-10-29 18:57:37 UTC",
      "description":"",
      "head_pipeline_id":null,
      "id":40927570,
      "iid":3,
      "last_edited_at":null,
      "last_edited_by_id":null,
      "merge_commit_sha":null,
      "merge_error":null,
      "merge_params":{
         "force_remove_source_branch":"0"
      },
      "merge_status":"can_be_merged",
      "merge_user_id":null,
      "merge_when_pipeline_succeeds":false,
      "milestone_id":null,
      "source_branch":"mrtest",
      "source_project_id":15002192,
      "state":"opened",
      "target_branch":"master",
      "target_project_id":15002192,
      "time_estimate":0,
      "title":"MR Test 1",
      "updated_at":"2019-10-29 21:06:47 UTC",
      "updated_by_id":3493733,
      "url":"https://gitlab.com/tgilkerson/tekton-intro/merge_requests/3",
      "source":{
         "id":15002192,
         "name":"tekton-intro",
         "description":"An intro to tekton ci/cd",
         "web_url":"https://gitlab.com/tgilkerson/tekton-intro",
         "avatar_url":null,
         "git_ssh_url":"git@gitlab.com:tgilkerson/tekton-intro.git",
         "git_http_url":"https://gitlab.com/tgilkerson/tekton-intro.git",
         "namespace":"Tony Gilkerson",
         "visibility_level":20,
         "path_with_namespace":"tgilkerson/tekton-intro",
         "default_branch":"master",
         "ci_config_path":null,
         "homepage":"https://gitlab.com/tgilkerson/tekton-intro",
         "url":"git@gitlab.com:tgilkerson/tekton-intro.git",
         "ssh_url":"git@gitlab.com:tgilkerson/tekton-intro.git",
         "http_url":"https://gitlab.com/tgilkerson/tekton-intro.git"
      },
      "target":{
         "id":15002192,
         "name":"tekton-intro",
         "description":"An intro to tekton ci/cd",
         "web_url":"https://gitlab.com/tgilkerson/tekton-intro",
         "avatar_url":null,
         "git_ssh_url":"git@gitlab.com:tgilkerson/tekton-intro.git",
         "git_http_url":"https://gitlab.com/tgilkerson/tekton-intro.git",
         "namespace":"Tony Gilkerson",
         "visibility_level":20,
         "path_with_namespace":"tgilkerson/tekton-intro",
         "default_branch":"master",
         "ci_config_path":null,
         "homepage":"https://gitlab.com/tgilkerson/tekton-intro",
         "url":"git@gitlab.com:tgilkerson/tekton-intro.git",
         "ssh_url":"git@gitlab.com:tgilkerson/tekton-intro.git",
         "http_url":"https://gitlab.com/tgilkerson/tekton-intro.git"
      },
      "last_commit":{
         "id":"a478305ea7ccf8a346d08fbbfe18364dec5c3cad",
         "message":"A nothing change",
         "timestamp":"2019-10-29T18:56:57Z",
         "url":"https://gitlab.com/tgilkerson/tekton-intro/commit/a478305ea7ccf8a346d08fbbfe18364dec5c3cad",
         "author":{
            "name":"Tony Gilkerson",
            "email":"tgilkerson@miletwo.us"
         }
      },
      "work_in_progress":false,
      "total_time_spent":0,
      "human_total_time_spent":null,
      "human_time_estimate":null,
      "assignee_ids":[

      ],
      "action":"update"
   },
   "labels":[
      {
         "id":12643334,
         "title":"test3",
         "color":"#428BCA",
         "project_id":15002192,
         "created_at":"2019-10-29 21:06:47 UTC",
         "updated_at":"2019-10-29 21:06:47 UTC",
         "template":false,
         "description":null,
         "type":"ProjectLabel",
         "group_id":null
      }
   ],
   "changes":{
      "updated_at":{
         "previous":"2019-10-29 21:06:47 UTC",
         "current":"2019-10-29 21:06:47 UTC"
      },
      "updated_by_id":{
         "previous":null,
         "current":3493733
      },
      "labels":{
         "previous":[

         ],
         "current":[
            {
               "id":12643334,
               "title":"test3",
               "color":"#428BCA",
               "project_id":15002192,
               "created_at":"2019-10-29 21:06:47 UTC",
               "updated_at":"2019-10-29 21:06:47 UTC",
               "template":false,
               "description":null,
               "type":"ProjectLabel",
               "group_id":null
            }
         ]
      }
   },
   "repository":{
      "name":"tekton-intro",
      "url":"git@gitlab.com:tgilkerson/tekton-intro.git",
      "description":"An intro to tekton ci/cd",
      "homepage":"https://gitlab.com/tgilkerson/tekton-intro"
   }
}
```

Example of updating the merge request via curl:

```bash
curl -H "PRIVATE-TOKEN: XXXXXXXXX" \
     --data "labels=test1"
     -X PUT https://gitlab.com/api/v4/projects/15002192/merge_requests/3 

curl -H "PRIVATE-TOKEN: XXXXXXXXX" \
     -H "Content-Type: application/json" \
     --data '{"body":"comment test 1"}' \
     -X POST https://gitlab.com/api/v4/projects/15002192/merge_requests/3/notes 
```

### Push Event

```json
{
   "object_kind":"push",
   "event_name":"push",
   "before":"9ef85ba26d410fab3a5d44da05ed8a853a500997",
   "after":"b434b07c4c5d3345cde63a139decf90f6053ee26",
   "ref":"refs/heads/master",
   "checkout_sha":"b434b07c4c5d3345cde63a139decf90f6053ee26",
   "message":null,
   "user_id":3493733,
   "user_name":"Tony Gilkerson",
   "user_username":"tgilkerson",
   "user_email":"",
   "user_avatar":"https://secure.gravatar.com/avatar/f4889b181d1324246d7edae67ca17e99?s=80\u0026d=identicon",
   "project_id":15002192,
   "project":{
      "id":15002192,
      "name":"tekton-intro",
      "description":"An intro to tekton ci/cd",
      "web_url":"https://gitlab.com/tgilkerson/tekton-intro",
      "avatar_url":null,
      "git_ssh_url":"git@gitlab.com:tgilkerson/tekton-intro.git",
      "git_http_url":"https://gitlab.com/tgilkerson/tekton-intro.git",
      "namespace":"Tony Gilkerson",
      "visibility_level":20,
      "path_with_namespace":"tgilkerson/tekton-intro",
      "default_branch":"master",
      "ci_config_path":null,
      "homepage":"https://gitlab.com/tgilkerson/tekton-intro",
      "url":"git@gitlab.com:tgilkerson/tekton-intro.git",
      "ssh_url":"git@gitlab.com:tgilkerson/tekton-intro.git",
      "http_url":"https://gitlab.com/tgilkerson/tekton-intro.git"
   },
   "commits":[
      {
         "id":"b434b07c4c5d3345cde63a139decf90f6053ee26",
         "message":"working gitlab push event\n",
         "timestamp":"2019-10-29T00:05:13Z",
         "url":"https://gitlab.com/tgilkerson/tekton-intro/commit/b434b07c4c5d3345cde63a139decf90f6053ee26",
         "author":{
            "name":"tgilkerson",
            "email":"tgilkerson@miletwo.us"
         },
         "added":[

         ],
         "modified":[
            "README.md",
            "exercises/ex4-triggers.yaml"
         ],
         "removed":[

         ]
      }
   ],
   "total_commits_count":1,
   "push_options":{

   },
   "repository":{
      "name":"tekton-intro",
      "url":"git@gitlab.com:tgilkerson/tekton-intro.git",
      "description":"An intro to tekton ci/cd",
      "homepage":"https://gitlab.com/tgilkerson/tekton-intro",
      "git_http_url":"https://gitlab.com/tgilkerson/tekton-intro.git",
      "git_ssh_url":"git@gitlab.com:tgilkerson/tekton-intro.git",
      "visibility_level":20
   }
}
```

